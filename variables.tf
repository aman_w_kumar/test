terraform {
  backend "gcs" {
    credentials = "key.json"
    bucket  = "test-tfstate124"
    prefix  = "terraform/state"
  }
}
